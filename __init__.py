# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import product
from . import shipment
from . import dash
from . import inventory


def register():
    Pool.register(
        product.Product,
        shipment.ShipmentInternal,
        shipment.AppShipmentInternal,
        inventory.AppInventory,
        dash.DashApp,
        inventory.Inventory,
        module='dash_stock', type_='model')
